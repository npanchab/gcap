package com.americanexpress.dc.bean;

import java.util.List;

public class Profile {
	private String holderSinceDate;
	private BirthDate birthDate;
	private String lastName;
	private Boolean isAccountLevelEmbossed;
	private String firstName;
	private Boolean isCompanyNameEmbossed;
	private String embossedName;
	private String nationalId;
	private List<String> traits;
	private String titleName;
	private String middleName;
	private String additionalLastName;
	private String suffixName;
	private String fullName;
	private String basicEmbossedName;
	private String additionalEmbossedName;
	private String gender;
	private String relativeName;
	private String maritalCode;
	private String income;
	private String employerName;

	public String getHolderSinceDate() {
		return holderSinceDate;
	}

	public void setHolderSinceDate(String holderSinceDate) {
		this.holderSinceDate = holderSinceDate;
	}

	public BirthDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(BirthDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getIsAccountLevelEmbossed() {
		return isAccountLevelEmbossed;
	}

	public void setIsAccountLevelEmbossed(Boolean isAccountLevelEmbossed) {
		this.isAccountLevelEmbossed = isAccountLevelEmbossed;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsCompanyNameEmbossed() {
		return isCompanyNameEmbossed;
	}

	public void setIsCompanyNameEmbossed(Boolean isCompanyNameEmbossed) {
		this.isCompanyNameEmbossed = isCompanyNameEmbossed;
	}

	public String getEmbossedName() {
		return embossedName;
	}

	public void setEmbossedName(String embossedName) {
		this.embossedName = embossedName;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public List<String> getTraits() {
		return traits;
	}

	public void setTraits(List<String> traits) {
		this.traits = traits;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAdditionalLastName() {
		return additionalLastName;
	}

	public void setAdditionalLastName(String additionalLastName) {
		this.additionalLastName = additionalLastName;
	}

	public String getSuffixName() {
		return suffixName;
	}

	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBasicEmbossedName() {
		return basicEmbossedName;
	}

	public void setBasicEmbossedName(String basicEmbossedName) {
		this.basicEmbossedName = basicEmbossedName;
	}

	public String getAdditionalEmbossedName() {
		return additionalEmbossedName;
	}

	public void setAdditionalEmbossedName(String additionalEmbossedName) {
		this.additionalEmbossedName = additionalEmbossedName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRelativeName() {
		return relativeName;
	}

	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}

	public String getMaritalCode() {
		return maritalCode;
	}

	public void setMaritalCode(String maritalCode) {
		this.maritalCode = maritalCode;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
}
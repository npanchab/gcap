package com.americanexpress.dc.bean;

public class LineOfBusiness {
	private Corporate corporate;
	private Company company;

	public Corporate getCorporate() {
		return corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}

package com.americanexpress.dc.bean;

public class DigitalInfo {
	private String digitalAssetId;
	private String seGenesisProductCode;
	private String legalEntityDesc;
	private String productCode;
	private String abbreviatedProductDesc;
	private String productDesc;
	private String productId;
	private String productShortDesc;
	private String productConfigCode;
	private String legalEntityCode;
	private String pctCode;
	private String secondaryProductType;
	private String plasticDeviceType;
	private String cardFeatureBundleCode;

	public String getDigitalAssetId() {
		return digitalAssetId;
	}

	public void setDigitalAssetId(String digitalAssetId) {
		this.digitalAssetId = digitalAssetId;
	}

	public String getSeGenesisProductCode() {
		return seGenesisProductCode;
	}

	public void setSeGenesisProductCode(String seGenesisProductCode) {
		this.seGenesisProductCode = seGenesisProductCode;
	}

	public String getLegalEntityDesc() {
		return legalEntityDesc;
	}

	public void setLegalEntityDesc(String legalEntityDesc) {
		this.legalEntityDesc = legalEntityDesc;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getAbbreviatedProductDesc() {
		return abbreviatedProductDesc;
	}

	public void setAbbreviatedProductDesc(String abbreviatedProductDesc) {
		this.abbreviatedProductDesc = abbreviatedProductDesc;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductShortDesc() {
		return productShortDesc;
	}

	public void setProductShortDesc(String productShortDesc) {
		this.productShortDesc = productShortDesc;
	}

	public String getProductConfigCode() {
		return productConfigCode;
	}

	public void setProductConfigCode(String productConfigCode) {
		this.productConfigCode = productConfigCode;
	}

	public String getLegalEntityCode() {
		return legalEntityCode;
	}

	public void setLegalEntityCode(String legalEntityCode) {
		this.legalEntityCode = legalEntityCode;
	}

	public String getPctCode() {
		return pctCode;
	}

	public void setPctCode(String pctCode) {
		this.pctCode = pctCode;
	}

	public String getSecondaryProductType() {
		return secondaryProductType;
	}

	public void setSecondaryProductType(String secondaryProductType) {
		this.secondaryProductType = secondaryProductType;
	}

	public String getPlasticDeviceType() {
		return plasticDeviceType;
	}

	public void setPlasticDeviceType(String plasticDeviceType) {
		this.plasticDeviceType = plasticDeviceType;
	}

	public String getCardFeatureBundleCode() {
		return cardFeatureBundleCode;
	}

	public void setCardFeatureBundleCode(String cardFeatureBundleCode) {
		this.cardFeatureBundleCode = cardFeatureBundleCode;
	}
}
package com.americanexpress.dc.bean;

import java.util.List;

public class Status {
	private String rawAccountLevelStatusCode;
	private String plasticEffectiveDate;
	private List<String> replacementReasons;
	private String cancelDate;
	private Integer daysPastDue;
	private String replacementReasonCode;
	private String cancelStatusChangeDate;
	private String expirationDate;
	private String accountSetupDate;
	private List<String> cardStatus;
	private Link link;
	private List<String> fosStatus;
	private String agingDate;
	private String accountReinstateDate;
	private String accountFirstUsedDate;
	private String accountReissueStatus;
	private Boolean isAccountIsolation;
	private String accountActivationRequired;
	private String rawCardLevelStatusCode;
	private String rawCardLevelStatusDesc;
	private String rawAccountLevelHighestSeverityStatusCode;
	private String accountConversionDate;

	public String getRawAccountLevelStatusCode() {
		return rawAccountLevelStatusCode;
	}

	public void setRawAccountLevelStatusCode(String rawAccountLevelStatusCode) {
		this.rawAccountLevelStatusCode = rawAccountLevelStatusCode;
	}

	public String getPlasticEffectiveDate() {
		return plasticEffectiveDate;
	}

	public void setPlasticEffectiveDate(String plasticEffectiveDate) {
		this.plasticEffectiveDate = plasticEffectiveDate;
	}

	public List<String> getReplacementReasons() {
		return replacementReasons;
	}

	public void setReplacementReasons(List<String> replacementReasons) {
		this.replacementReasons = replacementReasons;
	}

	public String getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Integer getDaysPastDue() {
		return daysPastDue;
	}

	public void setDaysPastDue(Integer daysPastDue) {
		this.daysPastDue = daysPastDue;
	}

	public String getReplacementReasonCode() {
		return replacementReasonCode;
	}

	public void setReplacementReasonCode(String replacementReasonCode) {
		this.replacementReasonCode = replacementReasonCode;
	}

	public String getCancelStatusChangeDate() {
		return cancelStatusChangeDate;
	}

	public void setCancelStatusChangeDate(String cancelStatusChangeDate) {
		this.cancelStatusChangeDate = cancelStatusChangeDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getAccountSetupDate() {
		return accountSetupDate;
	}

	public void setAccountSetupDate(String accountSetupDate) {
		this.accountSetupDate = accountSetupDate;
	}

	public List<String> getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(List<String> cardStatus) {
		this.cardStatus = cardStatus;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public List<String> getFosStatus() {
		return fosStatus;
	}

	public void setFosStatus(List<String> fosStatus) {
		this.fosStatus = fosStatus;
	}

	public String getAgingDate() {
		return agingDate;
	}

	public void setAgingDate(String agingDate) {
		this.agingDate = agingDate;
	}

	public String getAccountReinstateDate() {
		return accountReinstateDate;
	}

	public void setAccountReinstateDate(String accountReinstateDate) {
		this.accountReinstateDate = accountReinstateDate;
	}

	public String getAccountFirstUsedDate() {
		return accountFirstUsedDate;
	}

	public void setAccountFirstUsedDate(String accountFirstUsedDate) {
		this.accountFirstUsedDate = accountFirstUsedDate;
	}

	public String getAccountReissueStatus() {
		return accountReissueStatus;
	}

	public void setAccountReissueStatus(String accountReissueStatus) {
		this.accountReissueStatus = accountReissueStatus;
	}

	public Boolean getIsAccountIsolation() {
		return isAccountIsolation;
	}

	public void setIsAccountIsolation(Boolean isAccountIsolation) {
		this.isAccountIsolation = isAccountIsolation;
	}

	public String getAccountActivationRequired() {
		return accountActivationRequired;
	}

	public void setAccountActivationRequired(String accountActivationRequired) {
		this.accountActivationRequired = accountActivationRequired;
	}

	public String getRawCardLevelStatusCode() {
		return rawCardLevelStatusCode;
	}

	public void setRawCardLevelStatusCode(String rawCardLevelStatusCode) {
		this.rawCardLevelStatusCode = rawCardLevelStatusCode;
	}

	public String getRawCardLevelStatusDesc() {
		return rawCardLevelStatusDesc;
	}

	public void setRawCardLevelStatusDesc(String rawCardLevelStatusDesc) {
		this.rawCardLevelStatusDesc = rawCardLevelStatusDesc;
	}

	public String getRawAccountLevelHighestSeverityStatusCode() {
		return rawAccountLevelHighestSeverityStatusCode;
	}

	public void setRawAccountLevelHighestSeverityStatusCode(String rawAccountLevelHighestSeverityStatusCode) {
		this.rawAccountLevelHighestSeverityStatusCode = rawAccountLevelHighestSeverityStatusCode;
	}

	public String getAccountConversionDate() {
		return accountConversionDate;
	}

	public void setAccountConversionDate(String accountConversionDate) {
		this.accountConversionDate = accountConversionDate;
	}
}
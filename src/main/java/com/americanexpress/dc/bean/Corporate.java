package com.americanexpress.dc.bean;

public class Corporate {
	private String name;
	private String id;
	private String taxId;
	private String operationSegmentCode;
	private String employeeId;
	private String costCenter;
	private String individuallyBilledPaperOnoffNoticeEndDate;
	private String individuallyBilledPaperOnoffActionCode;
	private String iaCode;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getOperationSegmentCode() {
		return operationSegmentCode;
	}

	public void setOperationSegmentCode(String operationSegmentCode) {
		this.operationSegmentCode = operationSegmentCode;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getIndividuallyBilledPaperOnoffNoticeEndDate() {
		return individuallyBilledPaperOnoffNoticeEndDate;
	}

	public void setIndividuallyBilledPaperOnoffNoticeEndDate(String individuallyBilledPaperOnoffNoticeEndDate) {
		this.individuallyBilledPaperOnoffNoticeEndDate = individuallyBilledPaperOnoffNoticeEndDate;
	}

	public String getIndividuallyBilledPaperOnoffActionCode() {
		return individuallyBilledPaperOnoffActionCode;
	}

	public void setIndividuallyBilledPaperOnoffActionCode(String individuallyBilledPaperOnoffActionCode) {
		this.individuallyBilledPaperOnoffActionCode = individuallyBilledPaperOnoffActionCode;
	}

	public String getIaCode() {
		return iaCode;
	}

	public void setIaCode(String iaCode) {
		this.iaCode = iaCode;
	}
}
